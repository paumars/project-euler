let nb_adjacent = 4

let parse_in input =
  input
  |> String.split_on_char '\n'
  |> List.filter (fun line -> line <> String.empty)
  |> List.map (fun line ->
      line
      |> String.split_on_char ' '
      |> List.map int_of_string
      |> Array.of_list
    )
  |> Array.of_list

let check_adjacent mat i j di dj =
  let m, n = Array.length mat, Array.length mat.(0) in
  0 <= i + (nb_adjacent-1)*di && i + (nb_adjacent-1)*di < m &&
  0 <= j + (nb_adjacent-1)*dj && j + (nb_adjacent-1)*dj < n

let largest_grid_prod mat =
  let m, n = Array.length mat, Array.length mat.(0) in
  let directions = [
    (-1, -1); (* up left *)
    (-1, 0); (* up *)
    (-1, 1); (* up right *)
    (0, -1); (* left *)
    (0, 1); (* right *)
    (1, -1); (* down left *)
    (1, 0); (* down *)
    (1, 1) (* down right *)
  ]
  in
  let max_prod = ref 0 in
  for i = 0 to m - 1 do
    for j = 0 to n - 1 do
      List.iter (fun (di, dj) ->
        if check_adjacent mat i j di dj then
          let prod_candidate =
            mat.(i).(j)
            * mat.(i+di).(j+dj)
            * mat.(i+2*di).(j+2*dj)
            * mat.(i+3*di).(j+3*dj)
          in
          max_prod := max !max_prod prod_candidate
      ) directions
    done
  done;
  !max_prod

let () =
  if Array.length Sys.argv != 2 then
    raise (Invalid_argument "Input file required.")
  else
    let grid =
      Sys.argv.(1)
      |> fun file -> In_channel.with_open_text file In_channel.input_all
      |> parse_in
    in
    Printf.printf "Solution: %d\n" @@ largest_grid_prod grid
