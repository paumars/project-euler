let target_prime_index = 10001

let find_nth_prime n =
  let rec find_prime_at_index candidate count =
    if count = n then candidate
    else
      let next_candidate = candidate + 2 in
      if Prime.is_prime next_candidate then
        find_prime_at_index next_candidate (count + 1)
      else
        find_prime_at_index next_candidate count
  in
  if n = 1 then 2  (* Handle first prime specially *)
  else find_prime_at_index 1 1

let () =
  Printf.printf "Solution: %d\n" @@ find_nth_prime target_prime_index
