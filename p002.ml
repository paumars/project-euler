let threshold = 4000000

let sum_of_even_fibonacci_numbers threshold =
  let rec calculate_sum_of_even_fibonacci_numbers sum x y =
    if sum < threshold 
      then calculate_sum_of_even_fibonacci_numbers (sum + x + y) (x + 2 * y) (2 * x + 3 * y)
    else sum in
  calculate_sum_of_even_fibonacci_numbers 0 1 1

let () =
    let sum = sum_of_even_fibonacci_numbers threshold in
    Printf.printf "Solution: %d\n" sum
