let threshold = 2000000

let sum_primes threshold =
  let rec aux acc n =
    if n >= threshold then acc
    else
      if Prime.is_prime n then
        aux (n + acc) (n + 2)
      else
        aux acc (n + 2)
  in
  aux 2 3

let () =
  Printf.printf "Solution: %d\n" @@ sum_primes threshold
