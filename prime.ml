let is_prime n =
  (* Handle edge cases *)
  match n with
  | 1 -> false
  | 2 | 3 -> true
  | n when n mod 2 = 0 || n mod 3 = 0 -> false
  | n ->
      (* Check divisibility up to square root using wheel factorization *)
      let sqrt_n = n |> float_of_int |> sqrt |> floor |> int_of_float in
      let rec check_divisibility i =
        if i > sqrt_n then true
        else if n mod i = 0 || n mod (i + 2) = 0 then false
        else check_divisibility (i + 6)
      in
      check_divisibility 5
