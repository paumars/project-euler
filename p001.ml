let threshold = 1000

let multiples_of_3_or_5 threshold =
  let triangular_sum n = n * (n + 1) / 2 in
  let threshold = threshold - 1 in
  3 * triangular_sum(threshold / 3) + 5 * triangular_sum(threshold / 5) - 15 * triangular_sum(threshold / 15)

let () = 
  let sum = multiples_of_3_or_5 threshold in
  Printf.printf "Solution: %d\n" sum
