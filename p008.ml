let threshold = 13

let parse_in input =
  input
  |> Str.global_replace (Str.regexp "\n") ""
  |> String.to_seq
  |> List.of_seq
  |> List.map (fun c -> int_of_char c - int_of_char '0')

(* Extract a window of specified size from a list *)
let take (n: int) (lst: 'a list) : 'a list =
  let rec aux count acc = function
    | [] -> List.rev acc
    | x :: xs when count < n -> aux (count + 1) (x :: acc) xs
    | _ -> List.rev acc
  in
  aux 0 [] lst

(* Calculate product of all numbers in a list *)
let product (numbers: int list) : int =
  List.fold_left ( * ) 1 numbers

(* Find largest product of consecutive digits *)
let find_largest_product (window_size: int) (digits: int list) : int =
  let rec aux max_product = function
    | [] -> max_product
    | remaining when List.length remaining < window_size -> max_product
    | remaining ->
        let window = take window_size remaining in
        let current_product = product window in
        aux (max current_product max_product) (List.tl remaining)
  in
  aux 0 digits

let () =
  if Array.length Sys.argv != 2 then
    raise (Invalid_argument "Input file required.")
  else
    let number =
      Sys.argv.(1)
      |> fun file -> In_channel.with_open_text file In_channel.input_all
      |> parse_in
    in
    Printf.printf "Solution: %d\n" @@ find_largest_product threshold number
