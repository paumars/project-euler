let threshold = 20

let find_factors n =
  let rec bis n k l =
    if k > n then l
    else
      let last_factor = List.fold_left (fun acc elem ->
        if acc mod elem = 0 then acc / elem
        else acc
      ) k l in
      if last_factor > 1 then
        bis n (k + 1) (last_factor :: l)
      else
        bis n (k + 1) l
    in
    bis n 3 [2]


let lowest_common_multiple n =
  List.fold_left ( * ) 1 (find_factors n)
  

let () =
  Printf.printf "Solution: %d\n" @@ lowest_common_multiple threshold
