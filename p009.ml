let threshold = 1000

let find_triple_product s =
  let rec outer_loop a =
    if a <= (s-3)/3 then
      let rec inner_loop b =
        if b <= (s-1-a)/2 then
          let c = s - a - b in
          if c*c = a*a + b*b then a * b * c
          else inner_loop (b+1)
        else
          outer_loop (a+1)
      in
      inner_loop (a+1)
    else raise (Invalid_argument "Pythagorean triple not found.")
  in
  outer_loop 3

let () =
  Printf.printf "Solution: %d\n" @@ find_triple_product threshold
