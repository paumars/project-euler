let is_palindrome n =
 let rec bis n acc =
   if n = 0 then acc
   else
     let r = n mod 10 in
     bis (n / 10) (r :: acc)
 in
 let l = bis n [] in
 let l2 = l |> List.rev in
 List.equal (=) l l2


let rec first_for x master =
  let rec second_for y master =
    if y = 900 then master
    else
      let palin = x * y in
      if is_palindrome palin && palin > master then
        second_for (y - 1) palin
      else
        second_for (y - 1) master
  in
  let master' = second_for x master in
  if x = 900 then master'
  else first_for (x - 1) master'

let () =
  Printf.printf "Solution: %d\n" @@ first_for 999 0
