let threshold = 100

let sum_square_difference n =
  let sum_of_squares = n * (n + 1) * (2 * n + 1) / 6 in
  let sum_of_n = n * (n + 1) / 2 in
  sum_of_n * sum_of_n - sum_of_squares

let () =
  Printf.printf "Solution: %d\n" @@ sum_square_difference threshold
