let number = 600851475143

let largest_prime_factor n =
  let rec bis x k =
    if x mod k = 0 then
      let x' = x/k in
      if x' = 1 then
        x
      else
        bis x' k
    else
      bis x (k+1)
  in
  bis n 2

let () =
  Printf.printf "Solution: %d\n" @@ largest_prime_factor number
